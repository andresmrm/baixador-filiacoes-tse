# Baixador de listas de filiados em partididos políticos

Baixa dados de todos os partidos, todas as UFs.

## Instalação

Requer Python pelo menos versão 3.7 e Virtualenv:

    virtualenv env
    . env/bin/activate
    pip install -r requirements.txt

## Rodar

    ./main.py
