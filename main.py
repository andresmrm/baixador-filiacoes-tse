#!/usr/bin/env python3
# coding: utf-8

import io
import re
import zipfile
import itertools
import urllib.request
from pathlib import Path

import pandas as pd
from tqdm import tqdm
from bs4 import BeautifulSoup


# Página que contém os "selects" (HTML) com as listagens de partidos e UFs para
# baixar os ZIPs.
tse_lists = 'http://filiaweb.tse.jus.br/filiaweb/portal/relacoesFiliados.xhtml'
# Template do link para baixar cada ZIP.
tse_zip = 'http://agencia.tse.jus.br/estatistica/sead/eleitorado/filiados/uf/filiados_{cod}.zip'  # noqa
# Pasta onde salvar os CSVs
csvs_folder = Path('csvs')


def get_partidos_ufs():
    '''Extrai as listas de partidos e UFs a partir do site do TSE.'''
    html_text = urllib.request.urlopen(tse_lists).read().decode("utf8")
    tree = BeautifulSoup(html_text, 'html.parser')
    partidos = [opt.attrs['value'] for opt in tree.select('#partido > option')]
    ufs = [opt.attrs['value'] for opt in tree.select('#uf > option')]
    return partidos, ufs


def get_csvs(partido, uf, force=False):
    '''Baixa e extrai os CSVs de um partido em uma UF.'''
    cod = f'{partido}_{uf}'
    url = tse_zip.format(cod=cod)

    # pula arquivos que parece já ter sido baixados
    if list(csvs_folder.glob('_' + cod)) and not force:
        print('Partido-UF parece já baixado, pulando:', cod)
        return

    # baixa ZIP, descompacta e extrai os CSVs dentro deles
    with urllib.request.urlopen(url) as resp:
        with zipfile.ZipFile(io.BytesIO(resp.read())) as zip_obj:
            for csv_zip_filepath in zip_obj.namelist():
                if csv_zip_filepath.endswith('.csv'):
                    csv_filepath = csvs_folder / Path(csv_zip_filepath).name
                    csv_filepath.write_bytes(zip_obj.open(csv_zip_filepath).read())


def get_filiacoes(partidos, ufs):
    '''Baixa todos os CSVs com as filiações.'''
    csvs_folder.mkdir(parents=True, exist_ok=True)
    # gera uma lista com as combinações entre todos os partidos e UFs
    combinacoes = list(itertools.product(partidos, ufs))
    for partido, uf in tqdm(combinacoes):
        get_csvs(partido, uf)


def concat_csvs(output, include='.*', exclude='^$'):
    '''Concatena todos os CSVs baixados.'''
    with open(output, 'wb') as fout:
        header = False
        filepaths = [
            f for f in csvs_folder.iterdir()
            if re.search(include, f.name) and not re.search(exclude, f.name)
        ]
        for filepath in tqdm(filepaths):
            with open(filepath, 'rb') as f:
                # evita reescrever o header no meio do arquivo
                if header:
                    next(f)
                else:
                    header = True
                # copia o conteúdo do CSV
                for line in f:
                    fout.write(line)


def open_csvs(pattern):
    '''Lê CSVs com o nome segundo um padrão e concatena todos.'''
    return pd.concat((
        pd.read_csv(filepath, sep=';', encoding='latin1', dtype=str)
        for filepath in csvs_folder.glob(pattern)
    ), ignore_index=True)


if __name__ == '__main__':
    print('Baixando lista de partidos e UFs...')
    partidos, ufs = get_partidos_ufs()
    print(f'{len(partidos)} partidos:', partidos)
    print(f'{len(ufs)} UFs:', ufs)
    print('Baixando e extraindo CSVs...')
    get_filiacoes(partidos, ufs)
